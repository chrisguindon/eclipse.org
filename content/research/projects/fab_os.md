---
title: "FabOS"
date: 2020-01-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/fabos.png"
tags: ["Standard", "Industry 4.0"]
homepage: "https://www.fab-os.org/en/"
facebook: ""
linkedin: ""
twitter: ""
youtube: "https://www.youtube.com/watch?v=UZnhiqrv1iQ"
funding_bodies: ["bmwi"]
eclipse_projects: ["dt.basyx"]
project_topic: "I4.0"
summary: "The Industrie 4.0 Operating System"
hide_page_title: true
hide_sidebar: true
container: "bg-white"
header_wrapper_class: "header-projects-bg-img"
description: "# **FabOS**

**FabOS is the Industrie 4.0 Operating System.** The project develops an open, distributed, real-time, and secure operating system for the factory of the future. It will be the basis for data-driven services and AI applications in future smart factories.


Industry 4.0 promises to create the fourth industrial revolution and digitally transform production into smart manufacturing. New and ever-improving technologies are expected to improve flexibility and changeability for manufacturing systems. However, rigid, functionally separate IT architectures and fear of making data available for artificial intelligence (AI) applications still limit flexibility and use of data-driven technologies.


The FabOS research project aims to resolve these issues with an AI-focused operating system for production that is open, distributed, real-time capable, and secure. The project is funded by the German Federal Ministry of Economic Affairs and Energy (BMWi) as part of the 2019 AI innovation competition and combines the efforts of 22 partners, including the Eclipse Foundation, research institutions, universities, and companies. It is coordinated by Fraunhofer IPA.


The project is running from February 2020 to January 2023"
---

{{< grid/div class="container research-page-section" >}}

## FabOS Enables Cyber-Physical Production

The FabOS operating system should not be thought of like a PC operating system or comparable dedicated operating systems, but as a system of orchestrated components and services for the operation of a networked factory that consists of cyber-physical production systems. Together, these systems shape the factory as a cyber-physical production operation.

The production operating system forms the IT backbone for versatile automation in the factory of the future and the basis of an ecosystem for data-driven services and AI applications. The base framework of the operating system is the meta-kernel that provides the core functionality, similar to the kernel of an operating system.

FabOS integrates technologies that improve the real-time proximity of existing applications and enable and guarantee the hard-real-time capability and determinism of real-time systems in a modern and flexible infrastructure.

## Open Source and Open Standards Are Key Enablers for Open Systems
The FabOS project aims to follow the established principles of open source innovation, which includes an open architecture, integration of accepted standards, and strong stakeholder involvement. The project partners are developing an open architecture for FabOS and are implementing it as open source.

### Eclipse BaSyx
Open source draws its strength from being easy to integrate and reuse. Consequently, FabOS is not starting from scratch, but integrates and adapts existing solutions.

For example, [Eclipse BaSyx](https://www.eclipse.org/basyx/) will be an integral part of the FabOS ecosystem. Eclipse BaSyx is developed as part of [BaSys 4.2](https://www.basys40.de/), the continuation of the [BaSys 4.0](https://www.basys40.de/) research project. It implements key Industry 4.0 concepts, such as the [Asset Administration Shell](https://www.plattform-i40.de/PI40/Redaktion/EN/Downloads/Publikation/Details-of-the-Asset-Administration-Shell-Part1.html), and provides off-the-shelf components that enable quick setup of an Industry 4.0 infrastructure.

The FabOS project team also plans to integrate open source solutions from other domains, such as edge computing. That means FabOS will provide an open source infrastructure that is built on other open source components.

[FabOS flyer](https://www.fab-os.org/fileadmin/user_upload/FabOS-OnePager.pdf)

{{</ grid/div>}}

{{< grid/div class="bg-light-gray" isMarkdown="false">}}
{{< grid/div class="container research-page-section" >}}

# Consortium
## Research
* Fraunhofer-Institut für Produktionstechnik und Automatisierung (IPA), Suttgart
* Fraunhofer-Institut für Experimentelles Software Engineering (IESE), Kaiserslautern
* Fraunhofer‐Institut für Nachrichtentechnik, Heinrich‐Hertz‐Institut (HHI), Berlin
* Fraunhofer-Institut für Produktionstechnologie (IPT), Aachen
* Deutsches Forschungszentrum für Künstliche Intelligenz GmbH (DFKI), Kaiserslautern
* Institut für Intelligente Prozessautomation und Robotik IPR am KIT, Karlsruhe
* Institut für Steuerungstechnik der Werkzeugmaschinen und Fertigungseinrichtungen (ISW) der Universität Stuttgart

## Industry
* TRUMPF Werkzeugmaschinen GmbH + Co. KG - Germany
* Siemens AG (Berlin und München) - Germany
* Nokia Solutions and Networks GmbH & Co. KG - Germany
* Robert BOSCH GmbH - Germany
* NXP Semiconductors Germany GmbH - Germany
* Carl Zeiss 3D Automation GmbH - Germany
* BÄR Automation GmbH - Germany
* USU Software AG - Germany
* SYSGO AG - Germany
* SOTEC - Germany
* AI4BD Deutschland GmbH - Germany
* Advaneo GmbH - Germany
* Kenbun IT AG - Germany
* inno-focus - Germany
* Eclipse Foundation Europe GmbH - Germany
* ASCon Systems GmbH - Germany
* COSMO CONSULT DATA SCIENCE - Germany
* COMPAILE Solutions GmbH - Germany

{{</ grid/div>}}
{{</ grid/div>}}
